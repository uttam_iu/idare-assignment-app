// reducer of react redux
import { combineReducers } from 'redux';

const initialState = {
	csvData: '',
	page: ''
};

const csvDataReducer = (state = initialState, action) => {
	const data = (action && action.data) ? action.data : '';
	switch (action.type) {
		case 'upload': {
			return {
				csvData: data
			}
		}
		case 'download': {
			return state.csvData
		}
		default: return state
	}
};

const pageChangeReducer = (state = initialState, action) => {
	const data = (action && action.data) ? action.data : '';
	switch (action.type) {
		case 'change': {
			return {
				page: data
			}
		}
		default: return state
	}
};

export default combineReducers({ csvDataReducer, pageChangeReducer });
