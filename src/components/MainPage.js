// controlling for page_1 and page_2

import React from 'react';
import { connect } from 'react-redux';

const MainPage = props => {
	const { onPageChange, csvData } = props;

	return (
		<div>
			<div className='main-title'>Assignment </div>
			<div className='main-btn' onClick={() => onPageChange('page_1')}>upload file</div>
			{csvData && <div className='main-btn' onClick={() => onPageChange('page_2')}>view data</div>}
		</div>
	);
};

const mapDispatch = (dispatch) => {
	return {
		onPageChange: (pageName) => {
			dispatch({ type: 'change', data: pageName });
		}
	};
};

const mapStToPrps = (state) => {
	return {
		csvData: state.csvDataReducer.csvData
	};
};

export default connect(mapStToPrps, mapDispatch)(MainPage);
