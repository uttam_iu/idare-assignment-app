// generate print/pdf

import { forwardRef, useImperativeHandle } from 'react';
import jsPDF from 'jspdf';
import 'jspdf-autotable';

const PdfGenerator = (props, ref) => {
	const { csvData } = props;

	const getTableHeader = (str = csvData) => {
		const headers = str.slice(0, str.indexOf("\n")).split(',');
		let headerArr = [];
		headers.forEach(header => {
			const headData = header.replace(/_/g, ' ')
			headerArr.push(headData);
		});
		return [headerArr];
	};

	const getTableBody = (dataString = csvData) => {
		const headers = dataString.slice(0, dataString.indexOf("\n")).split(',');
		const lines = dataString
			.split(/\n/)
			.map(function (lineStr) {
				return lineStr.split(",");
			});

		const keys = lines[0];

		const objectsArr = lines
			.slice(1)
			.map(function (arr) {
				return arr.reduce(function (obj, val, i) {
					obj[keys[i]] = val;
					return obj;
				}, {});
			});

		let bodyArr = [];
		objectsArr.forEach(item => {
			let singleRow = [];
			headers.forEach(header => {
				singleRow.push(item[header]);
			});
			bodyArr.push(singleRow);
		})
		return bodyArr;
	};

	useImperativeHandle(ref, () => ({
		makePdf: (type) => {
			var doc = new jsPDF();
			doc.setFontSize(10)

			doc.autoTable({
				head: getTableHeader(),
				body: getTableBody(),
				startY: 10,
				showFoot: 'lastPage',
				headStyles: { fillColor: [210, 210, 210], textColor: 20, halign: 'center' },
				bodyStyles: { halign: 'center' }
			});

			if (type === 'pdf') doc.save(`${Date.now()}`);
			else {
				doc.autoPrint();
				doc.output('dataurlnewwindow');
			}
		}
	}));

	return null;
};

export default forwardRef(PdfGenerator);
