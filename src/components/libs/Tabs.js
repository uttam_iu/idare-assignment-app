// tabs for displaying csv to scatter/box plot and histogram

import React from 'react';
import { connect } from 'react-redux';
import Plotly from '../../plotly/CustomPlotly';

const Tabs = props => {
	const { csvData } = props;
	const openTab = (evt, identity) => {
		let i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("tabcontent");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(identity).style.display = "flex";
		evt.currentTarget.className += " active";
	};

	const scatterPlot = (axisObj) => {
		const trace1 = {
			x: axisObj.xAxis,
			y: axisObj.yAxis,
			mode: 'markers',
			type: 'scatter'
		};

		Plotly.newPlot('scatter_myDiv', [trace1], { autosize: true }, true, { width: "100%", height: "100%" });
	};

	const boxPlot = (axisObj) => {
		const trace1 = {
			y: axisObj.xAxis,
			type: 'box'
		};

		const trace2 = {
			y: axisObj.yAxis,
			type: 'box'
		};

		Plotly.newPlot('box_myDiv', [trace1, trace2], { autosize: true }, true, { width: "100%", height: "100%" });
	};

	const histogram = (axisObj) => {
		const trace = {
			x: axisObj.xAxis,
			type: 'histogram',
		};

		Plotly.newPlot('histo_myDiv', [trace], { autosize: true }, true, { width: "100%", height: "100%" });
	};

	const csvToArray = React.useCallback((str = '') => {
		const rows = str.slice(str.indexOf("\n") + 1).split("\n");

		let xAxis = [], yAxis = [];
		rows.map(function (row) {
			const values = row.split(',');
			xAxis.push(parseFloat(values[2]) ? parseFloat(values[2]) : 0);
			yAxis.push(parseFloat(values[3]) ? parseFloat(values[3]) : 0);
			return true;
		});

		return { xAxis, yAxis };
	}, []);

	React.useEffect(() => {
		const axisObj = csvToArray(csvData);
		scatterPlot(axisObj);
		boxPlot(axisObj);
		histogram(axisObj);
	}, [csvData, csvToArray]);

	return (
		<div className='tab-cont'>
			<div className="tab">
				<div className='tab-wra'>
					<button className="tablinks" onClick={(e) => openTab(e, 'scatter_plot')}>Scatter Plot</button>
					<button className="tablinks" onClick={(e) => openTab(e, 'box_plot')}>Box Plot</button>
					<button className="tablinks" onClick={(e) => openTab(e, 'histogram')}>Histogram</button>
				</div>
			</div>

			<div id="scatter_plot" className="tabcontent">
				<div className='tab-bdy' id='scatter_myDiv'></div>
			</div>

			<div id="box_plot" className="tabcontent">
				<div className='tab-bdy' id='box_myDiv'></div>
			</div>

			<div id="histogram" className="tabcontent">
				<div className='tab-bdy' id='histo_myDiv'></div>
			</div>
		</div>
	);
};

const mapStToPrps = (state) => {
	return {
		csvData: state.csvDataReducer.csvData
	};
};

export default connect(mapStToPrps)(Tabs);
