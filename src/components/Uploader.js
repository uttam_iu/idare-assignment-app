// page_1, upload csv file

import React from 'react';
import { connect } from 'react-redux';

const Uploader = props => {
	const { onPageChange, upload } = props;

	const onUpload = () => {
		const fileInpElm = document.getElementById('myfile');
		if (fileInpElm.files && fileInpElm.files[0]) {
			let GetFile = new FileReader();
			GetFile.onload = function () {
				upload(GetFile.result);
			};
			GetFile.readAsText(fileInpElm.files[0]);
		}
	};

	return (
		<div >
			<div className='back-btn' onClick={() => onPageChange('')}>
				&#8592; back
			</div>
			<div className='up-cont'>
				<div className='up-label padding-16 text-align-center'>CSV File Link</div>
				<div className='padding-16 text-align-center'>
					<input className='file-inp' type="file" id="myfile" accept='.csv' name="myfile" />
				</div>
				<div className='padding-16 text-align-center'>
					<button className='filled-btn' type="button" onClick={onUpload}>upload</button>
				</div>
			</div>
		</div>
	);
};

const mapDispatch = (dispatch) => {
	return {
		upload: (csvData) => {
			dispatch({ type: 'upload', data: csvData });
		},
		onPageChange: (pageName) => {
			dispatch({ type: 'change', data: pageName });
		}
	};
};

export default connect(null, mapDispatch)(Uploader);
