// page_2, show data and print/pdf/csv 

import React from 'react';
import { connect } from 'react-redux';
import PdfGenerator from './libs/PdfGenerator';
import Tabs from './libs/Tabs';

const Downloader = props => {
	const { csvData, onPageChange } = props;
	const pdfRef = React.useRef(null);

	const makeHtmlTable = React.useCallback(() => {
		if (csvData !== '') {
			let lines = csvData.split("\n"), output = [], i;
			for (i = 0; i < lines.length; i++)
				output.push("<tr style='height:40px;'><td style='padding:4px 8px;max-width:200px;border:1px solid rgba(0,0,0,.25);'>"
					+ lines[i].slice(0, -1).split(",").join("</td><td style='padding:4px 8px;max-width:200px;border:1px solid rgba(0,0,0,.25);'>")
					+ "</td></tr>");
			output = "<table style='display:flex;overflow:auto;border:1px solid rgba(0,0,0,.25); border-collapse:collapse'>" + output.join("") + "</table>";
			const div = document.getElementById('tableId');
			div.innerHTML = output;
		}
	}, [csvData]);

	React.useEffect(() => {
		makeHtmlTable();
	}, [makeHtmlTable]);

	const onSaveAs = (type = 'csv') => {
		if (type === 'csv') tableToCSV();
		else pdfRef.current.makePdf(type);
	};

	const tableToCSV = () => {
		const tableHeaders = Array.from(document.querySelectorAll('th'))
			.map(item => {
				const title = item.innerText.split("\n").filter(str => (str !== 0)).join(" ");
				return title;
			}).join(",");

		const rows = Array.from(document.querySelectorAll('tr'))
			.reduce((arr, currRow) => {
				if (currRow.querySelector('th')) return arr;

				const cells = Array.from(currRow.querySelectorAll('td'))
					.map(item => item.innerText)
					.join(',');
				return arr.concat([cells]);
			}, []);

		const csv = tableHeaders + '\n' + rows.join('\n');

		const csvFile = new Blob([csv], { type: 'text/csv' });
		const downloadLink = document.createElement('a');
		downloadLink.download = `${Date.now()}.csv`;
		downloadLink.href = window.URL.createObjectURL(csvFile);
		downloadLink.style.display = 'none';
		document.body.appendChild(downloadLink);

		downloadLink.click();
	};

	return (
		<div>
			<div className='back-btn' onClick={() => onPageChange('')}>
				&#8592; back
			</div>
			<div className='dw-cont'>
				<div className='dw-title'>My Data</div>
				<div className='dw-btn'>
					<div className='text-align-center p8'>
						<button className='filled-btn' type="button" onClick={() => onSaveAs('csv')}>download as csv</button>
					</div>
					<div className='text-align-center p8'>
						<button className='filled-btn' type="button" onClick={() => onSaveAs('pdf')}>download as pdf</button>
					</div>
				</div>
				<div className='text-align-center p8' style={{ textAlign: 'center' }}>
					<button className='filled-btn' type="button" onClick={() => onSaveAs('print')}>Print</button>
				</div>
				<div className='dw-center p8'>
					<div className='mt24' id='tableId' />
				</div>
				<PdfGenerator ref={pdfRef} csvData={csvData} />
				<Tabs />
			</div>
		</div>
	);
};

const mapStToPrps = (state) => {
	return {
		csvData: state.csvDataReducer.csvData
	};
};

const mapDispatch = (dispatch) => {
	return {
		onPageChange: (pageName) => {
			dispatch({ type: 'change', data: pageName });
		}
	};
};

export default connect(mapStToPrps, mapDispatch)(Downloader);
