const Plotly = require('plotly.js/lib/core');

// Load in the trace types for scatter, box and histogram
Plotly.register([
	require('plotly.js/lib/scatter'),
	require('plotly.js/lib/box'),
	require('plotly.js/lib/histogram')
]);

module.exports = Plotly;
