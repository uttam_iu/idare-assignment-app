import React from "react";
import "./resources/styles/style.scss";
import Uploader from './components/Uploader';
import Downloader from './components/Downloader';
import MainPage from './components/MainPage';
import { connect } from 'react-redux';

const App = (props) => {
	const { page } = props;
	return (
		<div className="idare-cont">
			{page === '' && <MainPage />}
			{page === 'page_1' && <Uploader />}
			{page === 'page_2' && <Downloader />}
		</div>
	);
};

const mapStToPrps = (state) => {
	return {
		page: state.pageChangeReducer.page
	};
};

export default connect(mapStToPrps)(App);
